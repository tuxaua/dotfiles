" Neovim automatically loads plugins in ~/.local/share/nvim/site/pack/default/start.
"
" The plugins are managed as git subtrees with respective remotes.
" To update a plugin, run the following commands from the repo's root directory:
"
" git fetch <plugin> master
" git subtree pull --prefix nvim/.local/share/nvim/site/pack/default/start/<plugin> <plugin> master -squash

set nocompatible

" Mode would be redundant because of airline, turn it off.
set noshowmode

" Show line numbers
set number relativenumber

" Highlight line with cursor
set cursorline

" Tab styles
"
" Default: Tabs are eight columns wide. Each indentation level is one tab.
set noexpandtab tabstop=8 softtabstop=8 shiftwidth=8

" Java: Each indentation level is four spaces. Tabs are not used.
autocmd FileType java setlocal autoindent expandtab softtabstop=4 shiftwidth=4

" Yaml: Each indentation level is two spaces. Tabs are not used. Cursor column.
autocmd FileType yaml setlocal autoindent expandtab softtabstop=2 shiftwidth=2 cursorcolumn

" Folding
set foldenable foldmethod=marker

" Search subdirectories
set path+=**

" Searching
set ignorecase

" Joining lines
set nojoinspaces

" Dealing with long lines (see *25.4*)
set linebreak
map <Up> gk
map <Down> gj

" Delete trailing whitespace on write
autocmd BufWritePre * %s/\s\+$//e

" Plugin gruvbox
set background=dark
" let g:gruvbox_contrast_dark='hard'
let g:gruvbox_invert_selection=0
colorscheme gruvbox
