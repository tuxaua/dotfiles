export PATH=$PATH:$HOME/.local/bin

# Preferred applications
export BROWSER=firefox
export EDITOR=vis
export PAGER=less
export READER=zathura
export TERMINAL=alacritty
export VISUAL=vis

# XDG directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# Application specific settings
export LESSHISTFILE="-"
export QT_STYLE_OVERRIDE=kvantum
